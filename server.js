const express = require("express");
const app = express();
const expressHandlerbars = require("express-handlebars");
const server = require("http").Server(app);
const io = require("socket.io")(server);
const path = require("path");
const port = process.env.PORT || 3000;

app.engine("handlebars", expressHandlerbars());
app.set("view engine","pug");

app.use("/public", express.static(path.join(__dirname, "public")));

server.listen(port,()=>{
    console.log("Server is booted...") 
});

app.get("/", (req, res)=>{
    res.render("index");
});

app.get("/onlineusers", (req, res)=>{
    // console.log(io.sockets.adapter.rooms);
    res.send(io.sockets.adapter.rooms);
});

io.on("connection", (socket)=>{

    
    console.log("user: "+ socket.id + " connected...");

    io.emit("user joined", socket.id);

    socket.on("chat.message", (message)=>{
        io.emit("chat.message", message)            
    });

    socket.on("user typing", (user)=>{
        io.emit("user typing", user)            
    });
    
    socket.on("user stop", (user)=>{
        io.emit("user stop", user)            
    });

    socket.on("disconnect", ()=>{
        console.log("user: "+ socket.id + " left...");

        socket.broadcast.emit("user left", socket.id)
    
    });
});

