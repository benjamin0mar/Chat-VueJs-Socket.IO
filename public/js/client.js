var socket = io();

new Vue({
  el: "#live_chat",
  data: {
    connectedUsers: [],
    messages: [],
    message: {
      "type": "",
      "action": "",
      "user": "",
      "text": "",
      "timestamp": ""
    },
    areTyping: []
  },
  created: function(){

    // actualizando la lista de usuarios conectados con ajax
    socket.on("user joined", function(socketId){
      axios.get('/onlineusers')
        .then(function(res){
          // actualizando la lista cuando un usuario se haya conectado
          for(var key in res.data){
            // agregamos si es que aun no esta en la lista
            if(this.connectedUsers.indexOf(key)<=-1){
              this.connectedUsers.push(key)    
            }
          }
          console.log(this.connectedUsers);
        }.bind(this));

      var infoMsg = {
        "type": "info",
        "msg": " Usuario: " + socket.id + " se unió" 
      };
      
      this.messages.push(infoMsg)

    }.bind(this));

    // actualizando la lista cuando un usuario se haya desconectado
    socket.on("user left", function(socketId){
      var index = this.connectedUsers.indexOf(socketId);
      // sacando de la lista de conectados
      if(index >= 0){
        this.connectedUsers.splice(index, 1);
      }

      var infoMsg = {
        "type": "info",
        "msg": " Usuario: " + socketId + " se salido" 
      };
      
      this.messages.push(infoMsg)

    }.bind(this));

    // recibir un nuevo mensaje y agregarlo a la lista de mensajes
    socket.on("chat.message", function(message){
      this.messages.push(message);
    }.bind(this));


    socket.on("user typing", function(socketId){
        this.areTyping.push(socketId);
    }.bind(this));


    socket.on("user stop", function(socketId){
      var index = this.areTyping.indexOf(socketId);
      // sacando de la lista de conectados
      if(index >= 0){
        this.areTyping.splice(index , 1);
      }
    }.bind(this));


  },
  methods: {
    send: function(){
      this.message.type = "chat";
      this.message.user = socket.id;
      this.message.timestamp = moment().calendar();
      socket.emit("chat.message", this.message);
      this.message.type = "";
      this.message.user = "";
      this.message.text = "";
      this.message.timestamp = "";
    },
    userIsTyping: function(user){
      if(this.areTyping.indexOf(user) >= 0 ){
        return true;
      }
      return false;
    },
    usersAreTyping: function(){
      if(this.areTyping.indexOf(socket.id) <=-1 ){
        this.areTyping.push(socket.id);
        socket.emit("user typing", socket.id);
      }
    },
    stoppedTyping: function(keycode){
      if(keycode == "13" || (keycode == "8" && this.message.text == '')){
        var index = this.areTyping.indexOf(socket.id)
        if(index >= 0){
          this.areTyping.splice(index, 1);
          socket.emit("user stop", socket.id);
        }
      }  
    }
  }

});